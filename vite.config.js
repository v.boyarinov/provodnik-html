import { resolve } from 'path'
import { defineConfig } from 'vite'

export default defineConfig({
  build: {
    rollupOptions: {
      input: {
        main: resolve(__dirname, 'index.html'),
        home: resolve(__dirname, './pages/home.html'),
        template: resolve(__dirname, './pages/template.html'),
      }
    }
  }
})