import { isVisible } from "./is-visible";

export function initAnimations() {
    const aboutAnimationWrapper = document.querySelector('.js-about-animation');

    if (aboutAnimationWrapper) {
        window.addEventListener('scroll', aboutAnimationHandler)
        
        function aboutAnimationHandler() {
            if (isVisible(aboutAnimationWrapper)) {
                aboutAnimationWrapper.classList.add('active');
                window.removeEventListener('scroll', aboutAnimationHandler)
            }
        }
    }
}

