import { setLayoutShift } from "./layout-shift";

export function toggleMenu() {
    const burger = document.querySelector('.js-burger');

    if (!burger) return;

    burger.addEventListener('click', (e) => {
        e.preventDefault();
        setLayoutShift();
        document.body.classList.toggle('menu-open');
    })
}