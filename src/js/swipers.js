import Swiper from "swiper";
import { Autoplay } from 'swiper/modules';

export function initSwipers() {
    const partnersSwipers = [...document.querySelectorAll('.js-partners-swiper')];
    const aboutSwiperContainer = document.querySelector('.js-about-swiper');

    let tablet = window.matchMedia('(min-width: 611px) and (max-width: 834px)');
    let desktop = window.matchMedia('(min-width: 835px)');
    let mobile = window.matchMedia('(min-width: 0) and (max-width: 610px)');

    if (partnersSwipers.length) {
        partnersSwipers.forEach((swiperContainer, index) => {
            swiperContainer.classList.add(`js-partners-swiper-${index}`)
            new Swiper(`.js-partners-swiper-${index}`, {
                slidesPerView: 'auto',
                loop: true,
                speed: 8000,
                autoplay: {
                    delay: 0,
                    disableOnInteraction: false,
                    reverseDirection: index % 2 === 1,
                },
                modules: [Autoplay],
            })
        })
    }

    if (aboutSwiperContainer) {
        let aboutSwiper = null;
        let aboutSwiperInit = false;

        function aboutSwiperMode() {
            if (mobile.matches) {
                if (aboutSwiperInit) return;
                aboutSwiperInit = true;
                aboutSwiper = new Swiper(aboutSwiperContainer, {
                    slidesPerView: 'auto',
                });
    
            } else if (tablet.matches || desktop.matches) {
                aboutSwiper?.destroy();
                aboutSwiperInit = false;
            }
        }    
        window.addEventListener('load', aboutSwiperMode);
    
        window.addEventListener('resize', aboutSwiperMode);    
    }
}